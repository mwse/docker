# Docker images

Several useful container base images

## `registry.gitlab.com/mwse/docker/deploy-image`

Enables deployments over ssh with docker-compose

Usage with *gitlab-ci*:

```yml
deploy:
  stage: deploy
  image: registry.gitlab.com/mwse/docker/deploy-image
  before_script:
    - eval $(ssh-agent -s)
    - setup-ssh $SSH_PRIVATE_KEY
```

## `registry.gitlab.com/mwse/docker/golang-devel`

Includes [`realize`](https://github.com/oxequa/realize) for running docker daemons.
Ready for live-reloading with docker volumes.

Example `docker-compose.yml`:

```yml
version: "3.5"

services:
  myapp:
    image: registry.gitlab.com/mwse/docker/golang-devel
    volumes:
      - .:/src
```

## `registry.gitlab.com/mwse/docker/golang-prod`

Basic scratch image for compiled golang binaries (no CGO).

Includes tzdata and ca-certs.

Example `Dockerfile`:

```Dockerfile
ARG VERSION=1.11
FROM golang:$VERSION as build

WORKDIR /src
COPY . .

ARG EXECUTABLE_NAME=main
RUN CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o $EXECUTABLE_NAME

FROM registry.gitlab.com/mwse/docker/golang-prod
COPY --from=build /src/$EXECUTABLE_NAME /bin/$EXECUTABLE_NAME
CMD ["main"]
```

## `registry.gitlab.com/mwse/docker/traefik-default`

Traefik configured with these properties:
- Usage with plain docker daemon
- HTTP->HTTPS redirect
- Automatic SSL via Letsencrypt

Example `docker-compose.yml`:
```yml
version: "3.5"

networks:
  ingress:
    name: global_ingress

services:
  ingress:
    image: registry.gitlab.com/mwse/docker/traefik-default
    command:
      - "--docker.network=global_ingress"
      - "--docker.domain=$INGRESS_BASE_DOMAIN"
      - "--acme.email=$INGRESS_ACME_EMAIL"
    networks:
      - ingress
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
      - "certs:/etc/traefik/acme/"
    restart: always

volumes:
  certs:
```
