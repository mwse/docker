#!/bin/sh

SSH_PRIVATE_KEY=$1
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
